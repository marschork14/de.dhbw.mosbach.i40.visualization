sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.MainMenu", {

		onInit: function() {
			this._getRouter().initialize();
		},

		onRexrothTilePressed: function(oEvent) {
			this._getRouter().navTo("rexroth");
		},

		onSensorTagTilePressed: function(oEvent) {
			this._getRouter().navTo("sensorTag");
		}
	});
});