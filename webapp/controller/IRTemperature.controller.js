sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.IRTemperature", {

		_intervalID: null,

		onInit: function() {

			var oController = this;
			var oRouter = this._getRouter();
			oRouter.getRoute("sensorTag").attachMatched(function() {
				if (oController._getModel("irTemperature").getProperty("/criticalNotification") === false) {
					oController.clearInt();
				}
			});
			oRouter.getRoute("irTemperature").attachMatched(function() {
				if (oController._intervalID === null) {
					oController._getSensorData(oController);
					oController._intervalID = setInterval(oController._getSensorData, oController._getModel("sensorTagSettings").getProperty(
						"/refreshRate") * 1000, oController);
				}
			});

		},

		_getSensorData: function(oController) {
			var odataModel = oController._getModel("odata");
			var temperatureModel = oController._getModel("irTemperature");
			var timeNow = new Date();
			var intervalMinute = Number(temperatureModel.getProperty("/intervalMinute"));
			var intervalHour = Number(temperatureModel.getProperty("/intervalHour"));
			//(minutes * seconds/minute)+hour*seconds/hour)*milliseconds/second
			var timeStart = new Date(timeNow.getTime() - (intervalMinute * 60 + intervalHour * 3600) * 1000);

			odataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", timeStart, timeNow), new sap.ui.model.Filter("C_SENSORNAME", "EQ",
					"Temperature (2)")],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)],
				success: function(oData) {
					if (oData.results.length > 0) {
						var oMinMaxAverage = oController._calculateMinMaxAverage(oData.results);
						temperatureModel.setProperty("/current", oMinMaxAverage.current);
						temperatureModel.setProperty("/minimum", oMinMaxAverage.minimum);
						temperatureModel.setProperty("/maximum", oMinMaxAverage.maximum);
						temperatureModel.setProperty("/average", oMinMaxAverage.average);
						oController._checkTemperature("current");
						oController._checkTemperature("average");
						oController._checkTemperature("minimum");
						oController._checkTemperature("maximum");
						oController._isCriticalTemperature(oData.results[0], "irTemperature");
					} else {
						oController._resetTileContentColor("current");
						oController._resetTileContentColor("average");
						oController._resetTileContentColor("minimum");
						oController._resetTileContentColor("maximum");
					}
				}
			});
		},

		restartInterval: function() {
			if (this._intervalID === null) {
				this._getSensorData(this);
				this._intervalID = setInterval(this._getSensorData, this._getModel("sensorTagSettings").getProperty("/refreshRate") * 1000, this);
			}
		},

		clearInt: function() {
			clearInterval(this._intervalID);
			this._intervalID = null;
		},

		onNotificationSelectionStateChanged: function() {
			if (this._getModel("irTemperature").getProperty("/criticalNotification")) {
				this.restartInterval();
			} else {
				this.clearInt();
			}
		},

		onTabSelected: function(oEvent) {
			//user don't want notifications
			if (!this._getModel("irTemperature").getProperty("/criticalNotification")) {
				switch (oEvent.getParameters("key").key) {
					case "1":
						this.restartInterval();
						break;
					default:
						this.clearInt();
				}
			}
		},

		onShowData: function() {

			var oResourceBundle = this._getResourceBundle();

			var oIntervalStart = this.getView().byId("dtpIntervalStart").getDateValue();
			var oIntervalEnd = this.getView().byId("dtpIntervalEnd").getDateValue();

			if (oIntervalStart > oIntervalEnd || oIntervalStart === null || oIntervalEnd === null) {
				var dialog = new sap.m.Dialog({
					title: oResourceBundle.getText("wrongTimeInterval"),
					type: 'Message',
					state: 'Error',
					content: new sap.m.Text({
						text: oResourceBundle.getText("wrongTimeIntervalMessage")
					}),
					beginButton: new sap.m.Button({
						text: oResourceBundle.getText("ok"),
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();
				return;
			}

			var oVizFrame = this.getView().byId("idcolumn");
			oVizFrame.destroyDataset();
			oVizFrame.destroyFeeds();

			var oModel = this.getView().getModel("odata");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: oResourceBundle.getText("time"),
					value: {
						path: "C_TIMESTAMP",
						formatter: function(sValue) {
							var timeFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
								pattern: "dd.MM. HH:mm:ss"
							});
							return timeFormat.format(sValue);
						}
					}
				}],

				measures: [{
					name: oResourceBundle.getText("temperature"),
					value: "{C_VALUE}"
				}],

				data: {
					path: "odata>" + this._getModel("config").getProperty("/tableName"),
					filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oIntervalStart, oIntervalEnd), new sap.ui.model.Filter("C_SENSORNAME",
						"EQ", "Temperature (2)")],
					sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", false)]
				}
			});

			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			oVizFrame.setVizType('line');

			//                4.Set Viz properties
			oVizFrame.setVizProperties({
				plotArea: {
					colorPalette: d3.scale.category20().range()
				},
				title: {
					text: oResourceBundle.getText("temperatureDiagramTitle")
				}

			});

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [oResourceBundle.getText("temperature")]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': [oResourceBundle.getText("time")]
				});
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		},

		_checkTemperature: function(sType) {
			var oTemperatureModel = this._getModel("irTemperature");
			var nAllowedMinTemp = Number(oTemperatureModel.getProperty("/allowedMin"));
			var nAllowedMaxTemp = Number(oTemperatureModel.getProperty("/allowedMax"));
			var nTemp = Number(oTemperatureModel.getProperty("/" + sType));
			var nCritical = (nAllowedMaxTemp - nAllowedMinTemp) / 10;

			var tileId = sType + "TemperatureTile";

			if (nTemp > nAllowedMaxTemp || nTemp < nAllowedMinTemp) {
				this.byId(tileId).setValueColor("Error");
				return;
			}

			if ((nAllowedMaxTemp - nTemp) < nCritical || (nTemp - nAllowedMinTemp) < nCritical) {
				this.byId(tileId).setValueColor("Critical");
				return;
			}
			this.byId(tileId).setValueColor("Good");
		},

		_resetTileContentColor: function(sType) {

			var oTileContent = this.byId(sType + "TemperatureTile");
			oTileContent.setValueColor("Neutral");
			oTileContent.setValue(0.0);
		}
	});
});