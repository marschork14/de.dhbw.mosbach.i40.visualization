sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("lehre.mosbach.dhbw.visualization.controller.BaseController", {

		onNavBackPressed: function() {
			history.go(-1);
		},

		/**
		 * Convenience method for accessing the router.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		_getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @public
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		_getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		_setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		_getResourceBundle: function() {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		_calculateMinMaxAverage: function(measurements) {

			if (measurements.length === 0) {
				return {
					current: 0.0,
					minimum: 0.0,
					maximum: 0.0,
					average: 0.0
				};
			}

			var min = Number.MAX_VALUE;
			var max = Number.MIN_VALUE;
			var sum = 0;
			var count = 0;

			jQuery.each(measurements, function(index, measurement) {
				var value = Number(measurement.C_VALUE);
				if (value < min) min = value;
				if (value > max) max = value;
				sum += value;
				count += 1;
			});

			return {
				current: Number(measurements[0].C_VALUE),
				minimum: min,
				maximum: max,
				average: sum / count
			};
		},

		_isCriticalTemperature: function(oCurrentValue, sModel) {

			var oTemperatureModel = this._getModel(sModel);
			var nAllowedMinTemp = Number(oTemperatureModel.getProperty("/allowedMin"));
			var nAllowedMaxTemp = Number(oTemperatureModel.getProperty("/allowedMax"));
			var nCurrentTemp = Number(oCurrentValue.C_VALUE);
			var bNotificationSelected = oTemperatureModel.getProperty("/criticalNotification");

			if ((nCurrentTemp < nAllowedMinTemp || nCurrentTemp > nAllowedMaxTemp) && bNotificationSelected) {
				emailjs.send("gmail", "notificationtemplate", {
					receiver: this._getModel("sensorTagSettings").getProperty("/email"),
					timestamp: sap.ui.core.format.DateFormat.getDateTimeInstance({
						style: "long"
					}).format(oCurrentValue.C_TIMESTAMP),
					sensor: sModel + " (SensorTag)",
					value: nCurrentTemp
				});
			}
		}

	});
});