sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.SensorTagSettings", {
		
		onEmailChanged: function(oEvent) {
			this._getModel("sensorTagSettings").setProperty("/email", oEvent.getParameters().value);
		}
	});
});