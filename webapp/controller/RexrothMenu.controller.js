sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.RexrothMenu", {
		
		onOpticalTilePressed: function(oEvent) {
			this._getRouter().navTo("optical");
		},

		onTactileTilePressed: function(oEvent) {
			this._getRouter().navTo("tactile");
		},

		onCapacitiveTilePressed: function(oEvent) {
			this._getRouter().navTo("capacitive");
		},

		onInductiveTilePressed: function(oEvent) {
			this._getRouter().navTo("inductive");
		}
	});
});