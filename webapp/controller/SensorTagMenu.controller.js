sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.SensorTagMenu", {

		onLightTilePressed: function(oEvent) {
			this._getRouter().navTo("light");
		},

		onTemperatureTilePressed: function(oEvent) {
			this._getRouter().navTo("temperature");
		},

		onIRTemperatureTilePressed: function(oEvent) {
			this._getRouter().navTo("irTemperature");
		},

		onHumidityTilePressed: function(oEvent) {
			this._getRouter().navTo("humidity");
		},
		onSettingsTilePressed: function(oEvent) {
			this._getRouter().navTo("sensorTagSettings");
		}
	});
});