sap.ui.define([
	"lehre/mosbach/dhbw/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("lehre.mosbach.dhbw.visualization.controller.Optical", {
		_intervalID: null,

		onInit: function() {

			var oController = this;
			var oRouter = this._getRouter();
			oRouter.getRoute("rexroth").attachMatched(function() {
					oController.clearInt();
				
			});
			oRouter.getRoute("optical").attachMatched(function() {
				if (oController._intervalID === null) {
					oController._getSensorData(oController);
					oController._intervalID = setInterval(oController._getSensorData, oController._getModel("rexrothSettings").getProperty(
						"/refreshRate") * 1000, oController);
				}
			});
		},

		_getSensorData: function(oController) {
			var odataModel = oController._getModel("odata");
			var temperatureModel = oController._getModel("optical");
			var timeNow = new Date();
			var intervalMinute = Number(temperatureModel.getProperty("/intervalMinute"));
			var intervalHour = Number(temperatureModel.getProperty("/intervalHour"));
			//(minutes * seconds/minute)+hour*seconds/hour)*milliseconds/second
			var timeStart = new Date(timeNow.getTime() - (intervalMinute * 60 + intervalHour * 3600) * 1000);

			odataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", timeStart, timeNow), new sap.ui.model.Filter("C_SENSORNAME", "EQ",
					"bSenOpt_i")],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)],
				success: function(oData) {
					if (oData.results.length > 0) {
						var oNumNegativesPositives = oController._calculateNumNegativesPositves(oData.results);
						temperatureModel.setProperty("/current", oNumNegativesPositives.current);
						temperatureModel.setProperty("/numNegatives", oNumNegativesPositives.numNegatives);
						temperatureModel.setProperty("/numPositives", oNumNegativesPositives.numPositives);
						oController._checkValues(oNumNegativesPositives.current);

					} else {
						oController._resetTileContentColor("current");
						oController._resetTileContentColor("numNegatives");
						oController._resetTileContentColor("numPositives");
					}
				}
			});
		},

		restartInterval: function() {
			if (this._intervalID === null) {
				this._getSensorData(this);
				this._intervalID = setInterval(this._getSensorData, this._getModel("rexrothSettings").getProperty("/refreshRate") * 1000, this);
			}
		},

		clearInt: function() {
			clearInterval(this._intervalID);
			this._intervalID = null;
		},

		onNotificationSelectionStateChanged: function() {
			if (this._getModel("optical").getProperty("/criticalNotification")) {
				this.restartInterval();
			} else {
				this.clearInt();
			}
		},

		onTabSelected: function(oEvent) {
			//user don't want notifications
			if (!this._getModel("optical").getProperty("/criticalNotification")) {
				switch (oEvent.getParameters("key").key) {
					case "1":
						this.restartInterval();
						break;
					default:
						this.clearInt();
				}
			}
		},

		onShowData: function() {

			var oResourceBundle = this._getResourceBundle();

			var oIntervalStart = this.getView().byId("dtpIntervalStart").getDateValue();
			var oIntervalEnd = this.getView().byId("dtpIntervalEnd").getDateValue();

			if (oIntervalStart > oIntervalEnd || oIntervalStart === null || oIntervalEnd === null) {
				var dialog = new sap.m.Dialog({
					title: oResourceBundle.getText("wrongTimeInterval"),
					type: 'Message',
					state: 'Error',
					content: new sap.m.Text({
						text: oResourceBundle.getText("wrongTimeIntervalMessage")
					}),
					beginButton: new sap.m.Button({
						text: oResourceBundle.getText("ok"),
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();
				return;
			}

			var odataModel = this.getView().getModel("odata");
			var resultObj = {
				"root": [{
					"dimension": oResourceBundle.getText("low"),
					"number": 0
				}, {
					"dimension": oResourceBundle.getText("high"),
					"number": 0
				}]
			};

			var numRequests = 0;
			var oController = this;

			odataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oIntervalStart, oIntervalEnd), new sap.ui.model.Filter("C_SENSORNAME",
					"EQ", "bSenOpt_i"), new sap.ui.model.Filter("C_VALUE", "EQ", "false")],
				success: function(oData) {

					resultObj.root[0].number = oData.results.length;
					numRequests++;
					if (numRequests === 2) {
						oController._createVizFrame(resultObj);
					}

				}

			});

			odataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oIntervalStart, oIntervalEnd), new sap.ui.model.Filter("C_SENSORNAME",
					"EQ", "bSenOpt_i"), new sap.ui.model.Filter("C_VALUE", "EQ", "true")],
				success: function(oData) {
					resultObj.root[1].number = oData.results.length;
					numRequests++;
					if (numRequests === 2) {
						oController._createVizFrame(resultObj);
					}
				}

			});

		},

		_createVizFrame: function(oResultObject) {
			var oResourceBundle = this._getResourceBundle();
			var oVizFrame = this.getView().byId("idcolumn");
			oVizFrame.destroyDataset();
			oVizFrame.destroyFeeds();

			var oModel = new sap.ui.model.json.JSONModel(oResultObject);
			this._setModel(oModel, "visual");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: oResourceBundle.getText("state"),
					value: "{dimension}"
				}],

				measures: [{
					name: oResourceBundle.getText("number"),
					value: "{number}"
				}],

				data: "{visual>/root}"
			});

			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			oVizFrame.setVizType('column');

			//                4.Set Viz properties
			oVizFrame.setVizProperties({
				plotArea: {
					colorPalette: d3.scale.category20().range()
				},
				title: {
					text: oResourceBundle.getText("sensorState")
				}

			});

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [oResourceBundle.getText("number")]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': [oResourceBundle.getText("state")]
				});
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		},

		_calculateNumNegativesPositves: function(measurements) {

			if (measurements.length === 0) {
				return {
					current: null,
					numNegatives: 0,
					numPositives: 0

				};
			}

			var sumNegatives = 0;
			var sumPositives = 0;

			jQuery.each(measurements, function(index, measurement) {
				var value = measurement.C_VALUE;
				if (value === "true") {
					sumPositives++;
				} else {
					sumNegatives++;
				}
			});

			var currentValue;

			if (measurements[0].C_VALUE === "true") {
				currentValue = "high";
			} else {
				currentValue = "low";
			}

			return {
				current: currentValue,
				numNegatives: sumNegatives,
				numPositives: sumPositives
			};
		},

		_checkValues: function(sValue) {
			this.byId("numNegativesTile").setValueColor("Error");
			this.byId("numPositivesTile").setValueColor("Good");

			if (sValue === "true") {
				this.byId("currentTile").setValueColor("Good");
			} else {
				this.byId("currentTile").setValueColor("Error");
			}
		},

		_resetTileContentColor: function(sType) {

			var oTileContent = this.byId(sType + "Tile");
			oTileContent.setValueColor("Neutral");
			oTileContent.setValue(0);
		}

	});
});